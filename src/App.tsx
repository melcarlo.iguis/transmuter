import React from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  Navigate,
} from "react-router-dom";
import Homepage from "./components/Homepage";
import { getDefaultProvider } from "ethers";
import {
  Mainnet,
  DAppProvider,
  useEtherBalance,
  useEthers,
  Config,
  ChainId,
  Kovan,
  Rinkeby,
} from "@usedapp/core";
import Header from "./components/Header";
import Main from "./components/Main";

const config: Config = {
  readOnlyChainId: Kovan.chainId,
  readOnlyUrls: {
    [Kovan.chainId]: getDefaultProvider("kovan"),
  },
  notifications: {
    expirationPeriod: 1000,
    checkInterval: 1000,
  },
};

function App() {
  return (
    <DAppProvider config={config}>
      <Router>
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/main" element={<Main />} />
        </Routes>
      </Router>
    </DAppProvider>
  );
}

export default App;
