import React from "react";
import Header from "./Header";
import btc from "../ico/btc.png";
import et from "../ico/et.png";
import xrp from "../ico/xrp.png";
import ttr from "../ico/tether.png";
import Footer from "./Footer";
import bg from "../ico/bg.jpg";
import rocket from "../ico/rockett.png";
import usd from "../ico/usd.png";
import { Container, Row, Col } from "react-bootstrap";
import Cards from "./Cards";
import { useNavigate } from "react-router-dom";
// animation
import "react-on-scroll-animation/build/index.css";
/*Animations are based on css files you can easily 
  overwriting it by you own rules, but you have to import 
  css files from build pack separately. 
  You can import or copy this file directly to your sass file as well.*/
import Rosa from "react-on-scroll-animation";

function Homepage() {
  const navigate = useNavigate();

  return (
    <div id="main-body">
      <img className="avatar" src={btc} alt="bitcoin-logo" />
      <img className="avatar2" src={et} alt="etherium-logo" />
      <img className="avatar3" src={ttr} alt="etherium-logo" />
      <Header />
      <div
        id="bg-image"
        style={{
          backgroundImage: `url(${bg})`,
          height: "100vh",
          position: "relative",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          zIndex: "-2",
        }}
      ></div>

      <div id="body-container">
        <p className="main-font m-0 p-0 main-text-animation header-font-size text-center">
          {" "}
          header one
        </p>
        <p className="secondary-font m-0 p-0 secondary-text-animation sub-header-font-size text-center">
          sub header
        </p>
        <div id="btn-swap" className="btn" onClick={() => navigate("/main")}>
          <p className="text">Swap Now</p>
        </div>
      </div>

      <div id="body-container2">
        <img src={rocket} alt="rocket" className="rocket"></img>
        <div className="container2-content-holder">
          <Container className="pt-4">
            <Row>
              <Col md={6} sm={12}>
                <Rosa animation={"fade-right"}>
                  <div id="card-holder">
                    <div id="main-card">
                      <Row>
                        <Col sm={6}>
                          <img className="card-coin-ico" src={usd} alt="usd" />
                          <p
                            className="text-uppercase secondary-font  p-0 m-0 pb-3 pt-3 text-center"
                            style={{ fontSize: "25px" }}
                          >
                            USDT
                          </p>
                        </Col>
                        <Col sm={6}>
                          <p
                            className="text-uppercase secondary-font p-0 m-0 pb-3 pt-3 text-center"
                            style={{ fontSize: "25px" }}
                          >
                            Stronghold
                          </p>
                        </Col>
                        <Col sm={12} className="px-5">
                          <div className="horizontal-line"></div>
                        </Col>
                        <Col sm={6} className="mx-auto px-5 py-0">
                          <p
                            className="text-uppercase secondary-font align-left pt-3 pb-0 text-left"
                            style={{ fontSize: "20px", paddingBottom: "0px" }}
                          >
                            APY
                            <p
                              className="text-uppercase secondary-font align-left text-left"
                              style={{ fontSize: "40px" }}
                            >
                              25.4%
                            </p>
                          </p>
                        </Col>
                        <Col sm={6} className="mx-auto px-5 py-0">
                          <p
                            className="text-uppercase secondary-font align-left pt-3 pb-0 text-left"
                            style={{ fontSize: "20px", paddingBottom: "0px" }}
                          >
                            TVL
                            <p
                              className="text-uppercase secondary-font align-left text-left"
                              style={{ fontSize: "40px" }}
                            >
                              36M
                            </p>
                          </p>
                        </Col>
                        <Col sm={12} className="px-5">
                          <div className="horizontal-line"></div>
                        </Col>
                        <Col sm={12} className="d-flex justify-content-center">
                          <div id="btn-swap2" className="btn">
                            <p className="text">Swap Now</p>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </Rosa>
              </Col>

              <Col md={6} sm={12}>
                <Rosa animation={"zoom-in"} delay={"300"}>
                  <div className="horizontal-line"></div>
                  <p className="secondary-font display-4 main-text-animation p-0 m-0">
                    Sample{" "}
                  </p>
                  <p className="secondary-font secondary-text-color p-0 m-0 pb-3">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nunc sed interdum augue. Duis varius leo lorem, et tincidunt
                    neque sollicitudin ac. Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. Donec diam orci, pellentesque
                    ut ornare sed, interdum id ante. Vestibulum dapibus justo
                    lacus, vitae tincidunt odio tristique quis. Aliquam diam
                    diam, tempor vel ullamcorper eu, consequat non massa.
                    Maecenas vulputate eu odio vitae elementum. Vestibulum
                    sodales blandit pharetra. Suspendisse auctor orci turpis, id
                    imperdiet turpis tincidunt quis. Morbi in tellus a ex
                    sodales tincidunt. Cras consequat nunc non pulvinar porta.
                  </p>
                  <div className="horizontal-line"></div>
                </Rosa>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <div id="body-container3">
        <Container className="position-relative" style={{ height: "100vh" }}>
          <div className="container3-content-holder">
            <Row>
              <Col md={6} sm={12}>
                <p className="secondary-font display-4 main-text-animation p-0 m-0">
                  Sample one{" "}
                </p>{" "}
                <p className="secondary-font secondary-text-color p-0 m-0 pb-3">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                  sed interdum augue. Duis varius leo lorem, et tincidunt neque
                  sollicitudin ac. Lorem ipsum dolor sit amet, consectetur
                  adipiscing elit. Donec diam orci, pellentesque ut ornare sed,
                  interdum id ante. Vestibulum dapibus justo lacus, vitae
                  tincidunt odio tristique quis. Aliquam diam diam, tempor vel
                  ullamcorper eu, consequat non massa. Maecenas vulputate eu
                  odio vitae elementum. Vestibulum sodales blandit pharetra.
                  Suspendisse auctor orci turpis, id imperdiet turpis tincidunt
                  quis. Morbi in tellus a ex sodales tincidunt. Cras consequat
                  nunc non pulvinar porta.
                </p>
              </Col>
              <Col md={6} sm={12}>
                {" "}
                <p className="secondary-font display-4 main-text-animation p-0 m-0">
                  Sample two{" "}
                </p>
                <p className="secondary-font secondary-text-color p-0 m-0 pb-3">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                  sed interdum augue. Duis varius leo lorem, et tincidunt neque
                  sollicitudin ac. Lorem ipsum dolor sit amet, consectetur
                  adipiscing elit. Donec diam orci, pellentesque ut ornare sed,
                  interdum id ante. Vestibulum dapibus justo lacus, vitae
                  tincidunt odio tristique quis. Aliquam diam diam, tempor vel
                  ullamcorper eu, consequat non massa. Maecenas vulputate eu
                  odio vitae elementum. Vestibulum sodales blandit pharetra.
                  Suspendisse auctor orci turpis, id imperdiet turpis tincidunt
                  quis. Morbi in tellus a ex sodales tincidunt. Cras consequat
                  nunc non pulvinar porta.
                </p>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
      <Footer />
    </div>
  );
}

export default Homepage;
