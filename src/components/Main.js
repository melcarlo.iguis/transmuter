/* eslint-disable spaced-comment */
/// <reference types="react-scripts"/>
import React from "react";
import { Container } from "@material-ui/core";
import Header from "./Header";
import { useEthers } from "@usedapp/core";
import helperConfig from "../helper-config.json";
import networkMapping from "../chain-info/deployments/map.json";
import { constants } from "ethers";
import brownieConfig from "../brownie-config.json";
import dapp from "../ico/dapp.png";
import { YourWallet } from "./yourWallet";
import { ClassNames } from "@emotion/react";
import { ClassificationTypeNames } from "typescript";
import { makeStyles } from "@mui/material";
import { textAlign } from "@mui/system";

export type Token = {
  image: string,
  address: string,
  name: string,
};

// const useStyles = makeStyles((theme) => ({
//   title: {
//     color: theme.palette.common.white,
//     textAlign: "center",
//     padding: theme.spacing(4),
//   },
// }));
function Main() {
  // show token values from the wallet
  // get the address of the different tokens
  // get the balance of the user wallet
  // send the brownie-config to our 'src' folder
  // send the build folder

  // const classes = useStyles;
  const { chainId, error } = useEthers();
  const netWorkName = chainId ? helperConfig[chainId] : "dev";

  const dappTokenAddress = chainId
    ? networkMapping[String(chainId)]["DappToken"][0]
    : constants.AddressZero;

  const wethTokenAddress = chainId
    ? brownieConfig["networks"][netWorkName]["weth_token"]
    : constants.AddressZero;

  const fauTokenAddress = chainId
    ? brownieConfig["networks"][netWorkName]["fau_token"]
    : constants.AddressZero;

  const supportedTokens: Array<Token> = [
    {
      image: dapp,
      address: dappTokenAddress,
      name: "DAPP",
    },
    {
      image: dapp,
      address: wethTokenAddress,
      name: "WETH",
    },
    {
      image: dapp,
      address: fauTokenAddress,
      name: "DAI",
    },
  ];
  return (
    <>
      <Header />
      <Container maxWidth="lg" className="mb-4">
        <div id="main-container">
          <h2>Dapp Token App</h2>
          <YourWallet supportedTokens={supportedTokens} />
        </div>
      </Container>
    </>
  );
}

export default Main;
