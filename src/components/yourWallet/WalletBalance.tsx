import { useEthers, useTokenBalance, useEtherBalance } from "@usedapp/core";
import { Token } from "../Main";
import { formatUnits } from "@ethersproject/units";
import { BalanceMsg } from "../BalanceMsg";
import { formatEther } from "@ethersproject/units";

export interface WalletBalanceProps {
  token: Token;
}

export const WalletBalance = ({ token }: WalletBalanceProps) => {
  const { image, address, name } = token;

  const { account } = useEthers();
  const tokenBalance = useTokenBalance(address, account);
  const etherBalance = useEtherBalance(account);
  const formattedTokenBalance: number = tokenBalance
    ? parseFloat(formatUnits(tokenBalance, 18))
    : 0;

  const foramattedEtherBalance: number = etherBalance
    ? parseFloat(formatUnits(etherBalance, 18))
    : 0;

  return (
    <>
      <p>ETH staking contract holds : {foramattedEtherBalance} ETH</p>
      <BalanceMsg
        label={`Your un-staked ${name} balance`}
        tokenImgSrc={image}
        amount={formattedTokenBalance}
      />
    </>
  );
};
