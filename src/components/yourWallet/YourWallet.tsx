import React, { useState } from "react";
import { Box } from "@material-ui/core";
import { TabContext, TabList, TabPanel } from "@material-ui/lab";
import { Token } from "../Main";
import { Tab } from "@material-ui/core";
import { formatEther } from "@ethersproject/units";
import { WalletBalance } from "./WalletBalance";
import { StakeForm } from "./StakeForm";
import networkMapping from "../../chain-info/deployments/map.json";
interface YourWalletProps {
  supportedTokens: Array<Token>;
}

export const YourWallet = ({ supportedTokens }: YourWalletProps) => {
  const [selectedTokenIndex, setSelectedTokenIndex] = useState<number>(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
    setSelectedTokenIndex(parseInt(newValue));
  };

  return (
    <Box>
      <h1>Your Wallet</h1>
      <Box id="wallet-container">
        <TabContext value={selectedTokenIndex.toString()}>
          <TabList onChange={handleChange} aria-label="stake from tabs">
            {supportedTokens.map((token, index) => {
              return (
                <Tab label={token.name} value={index.toString()} key={index} />
              );
            })}
          </TabList>
          {supportedTokens.map((token, index) => {
            return (
              <TabPanel value={index.toString()} key={index}>
                <div className="position-relative">
                  <div id="wallet-balance-text">
                    <WalletBalance
                      token={supportedTokens[selectedTokenIndex]}
                    />
                  </div>
                  <StakeForm token={supportedTokens[selectedTokenIndex]} />
                </div>
              </TabPanel>
            );
          })}
        </TabContext>
      </Box>
    </Box>
  );
};
