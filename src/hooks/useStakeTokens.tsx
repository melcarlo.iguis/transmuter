import { useContractFunction, useEthers } from "@usedapp/core";
import { constants, utils } from "ethers";
import { Contract } from "@ethersproject/contracts";
import TokenFarm from "../chain-info/contracts/TokenFarm.json";
import ERC20 from "../chain-info/contracts/MockERC20.json";
import netWorkMapping from "../chain-info/deployments/map.json";
import { useState, useEffect } from "react";

export const useStakeTokens = (tokenAddress: string) => {
  // address
  //   abi
  //   chainId
  const { chainId } = useEthers();
  const { abi } = TokenFarm;
  const tokenFarmAddress = chainId
    ? netWorkMapping[String(chainId)]["TokenFarm"][0]
    : constants.AddressZero;
  const tokenFarmInterface = new utils.Interface(abi);
  const tokenFarmContract = new Contract(tokenFarmAddress, tokenFarmInterface);

  const erc20ABI = ERC20.abi;
  const erc20Interface = new utils.Interface(erc20ABI);
  const er20Contract = new Contract(tokenAddress, erc20Interface);

  console.log(tokenFarmContract);
  console.log(er20Contract);

  // approve
  const { send: approveEr20Send, state: approveAndStakeErc20State } =
    useContractFunction(er20Contract, "approve", {
      transactionName: "Approve ERC20 transfer",
    });

  const approveAndStake = (amount: string) => {
    setAmountToStake(amount);
    return approveEr20Send(tokenFarmAddress, amount);
  };

  const { send: stakeSend, state: stakeState } = useContractFunction(
    tokenFarmContract,
    "stakeTokens",
    { transactionName: "Stake Tokens" }
  );

  const [amountToStake, setAmountToStake] = useState("0");
  // useEffect
  useEffect(() => {
    if (approveAndStakeErc20State.status === "Success") {
      // stake function
      console.log("stake");
      stakeSend(amountToStake, tokenAddress);
    }
  }, [approveAndStakeErc20State, tokenAddress, amountToStake]);

  const [state, setState] = useState(approveAndStakeErc20State);

  useEffect(() => {
    if (approveAndStakeErc20State.status === "Success") {
      setState(stakeState);
    } else {
      setState(approveAndStakeErc20State);
    }
  }, [approveAndStakeErc20State, stakeState]);

  return { approveAndStake, approveAndStakeErc20State };

  // stake token
};
